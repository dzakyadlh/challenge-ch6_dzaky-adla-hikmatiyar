const express = require("express");
const app = express();
const { user_game } = require("./models");
const { user_game_biodata } = require("./models");
const { user_game_history } = require("./models");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.post("/user", (req, res) => {
  const {
    email,
    password,
    first_name,
    last_name,
    date_of_birth,
    city,
    history,
  } = req.body;
  user_game
    .create({
      email,
      password,
    })
    .then((user) => {
      user_game_history.create({ history, user_id: user.id }).then((user) => {
        console.log({ message: "Create User Success", data: user });
      });
      user_game_biodata
        .create({
          first_name,
          last_name,
          date_of_birth,
          city,
          user_id: user.id,
        })
        .then((user) => {
          console.log({ message: "Create Biodata Success", data: user });
        });
      res.json({ message: "Create Success" });
    });
});

app.post("/history", (req, res) => {
  const { history, user_id } = req.body;
  user_game_history
    .create({
      history,
      user_id,
    })
    .then((history) => {
      res.json({ message: "Create Success", data: history });
    })
    .catch((err) => {
      console.log(err);
    });
});

app.get("/user", (req, res) => {
  user_game
    .findAll({
      include: [
        { model: user_game_biodata, as: "user_biodata" },
        { model: user_game_history, as: "user_history" },
      ],
    })
    .then((user) => {
      res.json({
        message: "Get data success",
        data: user,
      });
    });
});

app.get("/user/:id", (req, res) => {
  const { id } = req.params;
  user_game
    .findOne(
      { where: { id: id } },
      {
        include: [
          { model: user_game_biodata, as: "user_biodata" },
          { model: user_game_history, as: "user_history" },
        ],
      }
    )
    .then((user) => {
      res.json({ message: "Get data success", data: user });
    });
});

app.put("/user/:id", (req, res) => {
  const { id } = req.params;
  const { email, password, first_name, last_name, date_of_birth, city } =
    req.body;
  user_game.update({ email: email, password: password }, { where: { id: id } });
  user_game_biodata.update(
    {
      first_name: first_name,
      last_name: last_name,
      date_of_birth: date_of_birth,
      city: city,
    },
    { where: { user_id: id } }
  );
  res.json({ message: "Update success" });
});

app.delete("/user/:id", (req, res) => {
  const { id } = req.params;
  user_game.destroy({ where: { id: id } });
  user_game_biodata.destroy({ where: { user_id: id } });
  user_game_history
    .destroy({ where: { user_id: id } })
    .then(() => {
      res.json({ message: "Delete success" });
    })
    .catch((err) => {
      res.json({ message: "Delete failed" });
      console.log(err);
    });
});

app.listen(8000, () => console.log(`Listening at http://localhost:${8000}`));

// npx sequelize-cli migration:create --name modify_add_new_fields
// return Promise.all([
//       queryInterface.addColumn("Articles", "user_id", {
//         type: Sequelize.INTEGER,
//         allowNull: true,
//       }),
//     ]);

// queryInterface.renameColumn("Articles", "old_column", "new_column"),
